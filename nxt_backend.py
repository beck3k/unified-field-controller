import socketio

sio = socketio.Server()
app = socketio.WSGIApp(sio, static_files={
    '/': {'content_type': 'text/html', 'filename': 'backend.html'}
})


@sio.event
def my_event(sid, data):
    print data
    pass

